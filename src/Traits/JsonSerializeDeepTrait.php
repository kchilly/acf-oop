<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Traits;


use JsonSerializable;

trait JsonSerializeDeepTrait
{
    protected function jsonSerializeDeep(array $elements): array
    {
        $jsonSerialized = [];
        foreach ($elements as $element) {
            $jsonSerialized[] = $element instanceof JsonSerializable
                ? $element->jsonSerialize()
                : $element;
        }

        return $jsonSerialized;
    }
}
