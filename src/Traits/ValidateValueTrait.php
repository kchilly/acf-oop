<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Traits;


use InvalidArgumentException;

trait ValidateValueTrait
{
    /**
     * Validates the given value.
     *
     * @param string $valueType The value type for the error message
     * @param $value
     * @param array $availableValues
     *
     * @throws InvalidArgumentException
     */
    protected function validateValue(string $valueType, $value, array $availableValues): void
    {
        if (in_array($value, $availableValues, true) === false) {
            throw new InvalidArgumentException(sprintf(
                'The given "%s" %s is not valid! Available: "%s"',
                $value,
                $valueType,
                implode('", "', $availableValues)
            ));
        }
    }

    /**
     * Validates the given range.
     *
     * @param string $smallerValueName
     * @param int $smallerValue
     * @param string $biggerValueName
     * @param int $biggerValue
     */
    protected function validateIntRange(string $smallerValueName, int $smallerValue, string $biggerValueName, int $biggerValue): void
    {
        if ($smallerValue > $biggerValue) {
            throw new InvalidArgumentException(sprintf(
                'The given %s value (%s) needs to be greater than the %s value (%s)!',
                $biggerValueName,
                $biggerValue,
                $smallerValueName,
                $smallerValue
            ));
        }
    }
}
