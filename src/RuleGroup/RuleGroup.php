<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\RuleGroup;


use Csoft\AcfOOP\Rule\RuleInterface;
use Csoft\AcfOOP\Traits\JsonSerializeDeepTrait;
use JsonSerializable;

class RuleGroup implements JsonSerializable
{
    use JsonSerializeDeepTrait;

    /** @var RuleInterface[] */
    private array $rules = [];

    /**
     * Adds a new rule in AND relation.
     *
     * @param RuleInterface $rule
     *
     * @return $this
     */
    public function addRule(RuleInterface $rule): self
    {
        $this->rules[] = $rule;

        return $this;
    }

    public function jsonSerialize()
    {
        return $this->jsonSerializeDeep($this->rules);
    }
}
