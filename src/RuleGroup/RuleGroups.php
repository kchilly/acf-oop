<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\RuleGroup;


use Csoft\AcfOOP\Rule\RuleInterface;
use Csoft\AcfOOP\Traits\JsonSerializeDeepTrait;

class RuleGroups implements RuleGroupsInterface
{
    use JsonSerializeDeepTrait;

    /** @var RuleInterface[] */
    private array $ruleGroups = [];

    /**
     * Adds a new rule group in OR relation.
     *
     * @param RuleGroup $ruleGroup
     *
     * @return $this
     */
    public function addRuleGroup(RuleGroup $ruleGroup): self
    {
        $this->ruleGroups[] = $ruleGroup;

        return $this;
    }

    public function jsonSerialize()
    {
        return $this->jsonSerializeDeep($this->ruleGroups);
    }
}
