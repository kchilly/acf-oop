<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Rule;


use JsonSerializable;

interface RuleInterface extends JsonSerializable
{
    public const EQUALS = '==';
    public const NOT_EQUALS = '!=';
    public const LESS_THAN = '<';
    public const GREATER_THAN = '>';
}
