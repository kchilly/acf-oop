<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Rule;


class PostTypeRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    protected function getType(): string
    {
        return 'post_type';
    }
}
