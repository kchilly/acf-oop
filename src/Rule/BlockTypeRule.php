<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Rule;


class BlockTypeRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public function __construct(string $operator, string $value)
    {
        parent::__construct($operator, 'acf/' . $value);
    }

    /**
     * @inheritDoc
     */
    protected function getType(): string
    {
        return 'block';
    }
}
