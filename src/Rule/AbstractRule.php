<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Rule;


use PhpWedge\Core\JsonSerializable\JsonSerializableTrait;

abstract class AbstractRule implements RuleInterface
{
    use JsonSerializableTrait;

    protected string $param;
    protected string $operator;
    protected string $value;

    /**
     * AbstractLocation constructor.
     *
     * @param string $operator Operator string (Please use class constants like RuleInterface::EQUALS)
     * @param string $value
     */
    public function __construct(string $operator, string $value)
    {
        $this->param = $this->getType();
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * Returns the location type. (param in the array structure)
     *
     * @return string
     */
    abstract protected function getType(): string;
}
