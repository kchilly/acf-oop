<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Location;


use Csoft\AcfOOP\Rule\PostTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;

class PostTypeLocation extends AbstractLocation
{
    public function __construct(string $value)
    {
        $this->setRuleGroups(
            new PostTypeRule(RuleInterface::EQUALS, $value)
        );
    }
}
