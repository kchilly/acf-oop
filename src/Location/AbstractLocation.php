<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Location;


use Csoft\AcfOOP\Rule\BlockTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;
use Csoft\AcfOOP\RuleGroup\RuleGroup;
use Csoft\AcfOOP\RuleGroup\RuleGroups;
use Csoft\AcfOOP\RuleGroup\RuleGroupsInterface;
use Csoft\AcfOOP\Traits\JsonSerializeDeepTrait;

abstract class AbstractLocation implements RuleGroupsInterface
{
    use JsonSerializeDeepTrait;

    protected RuleGroupsInterface $ruleGroups;

    protected function setRuleGroups(RuleInterface $rule): void
    {
        $this->ruleGroups = (new RuleGroups())->addRuleGroup(
            (new RuleGroup())->addRule($rule)
        );
    }

    public function jsonSerialize()
    {
        return $this->jsonSerializeDeep([$this->ruleGroups])[0];
    }
}
