<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Location;


use Csoft\AcfOOP\Rule\BlockTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;

class BlockTypeLocation extends AbstractLocation
{
    public function __construct(string $value)
    {
        $this->setRuleGroups(
            new BlockTypeRule(RuleInterface::EQUALS, $value)
        );
    }
}
