<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\FieldGroup;


use Csoft\AcfOOP\Field\FieldInterface;
use Csoft\AcfOOP\RuleGroup\RuleGroupsInterface;
use Csoft\AcfOOP\Traits\ValidateValueTrait;
use JsonSerializable;
use PhpWedge\Core\JsonSerializable\JsonSerializableSnakeCaseTrait;

class FieldGroup implements JsonSerializable
{
    use JsonSerializableSnakeCaseTrait;
    use ValidateValueTrait;

    public const POSITION_NORMAL = 'normal';
    public const POSITION_SIDE = 'side';
    public const POSITION_ACF_AFTER_TITLE = 'acf_after_title';

    public const STYLE_DEFAULT = 'default';
    public const STYLE_SEAMLESS = 'seamless';

    public const LABEL_PLACEMENT_TOP = 'top';
    public const LABEL_PLACEMENT_LEFT = 'left';

    public const INSTRUCTION_PLACEMENT_LABEL = 'label';
    public const INSTRUCTION_PLACEMENT_FIELD = 'field';

    private string $key;
    private string $title;
    private array $fields = [];
    private RuleGroupsInterface $location;
    private int $menuOrder;
    private string $position;
    private string $style;
    private string $labelPlacement;
    private string $instructionPlacement;
    private array $hideOnScreen;

    /**
     * FieldGroup constructor.
     *
     * @param string $key
     * @param string $title
     * @param RuleGroupsInterface $location
     */
    public function __construct(string $key, string $title, RuleGroupsInterface $location)
    {
        $this->key = 'group_' . $key;
        $this->title = $title;
        $this->location = $location;
    }

    /**
     * @param FieldInterface[] $fields
     *
     * @return $this
     */
    public function setFields(array $fields): self
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @param int $menuOrder
     *
     * @return $this
     */
    public function setMenuOrder(int $menuOrder): self
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /**
     * @param string $position
     *
     * @return $this
     */
    public function setPosition(string $position): self
    {
        $available = [
            self::POSITION_NORMAL,
            self::POSITION_SIDE,
            self::POSITION_ACF_AFTER_TITLE,
        ];

        $this->validateValue('position', $position, $available);
        $this->position = $position;

        return $this;
    }

    /**
     * @param string $style
     *
     * @return $this
     */
    public function setStyle(string $style): self
    {
        $available = [
            self::STYLE_DEFAULT,
            self::STYLE_SEAMLESS,
        ];

        $this->validateValue('style', $style, $available);
        $this->style = $style;

        return $this;
    }

    /**
     * @param string $labelPlacement
     *
     * @return $this
     */
    public function setLabelPlacement(string $labelPlacement): self
    {
        $available = [
            self::LABEL_PLACEMENT_LEFT,
            self::LABEL_PLACEMENT_TOP,
        ];

        $this->validateValue('label placement', $labelPlacement, $available);
        $this->labelPlacement = $labelPlacement;

        return $this;
    }

    /**
     * @param string $instructionPlacement
     *
     * @return $this
     */
    public function setInstructionPlacement(string $instructionPlacement): self
    {
        $available = [
            self::INSTRUCTION_PLACEMENT_FIELD,
            self::INSTRUCTION_PLACEMENT_LABEL,
        ];

        $this->validateValue('instruction placement', $instructionPlacement, $available);
        $this->instructionPlacement = $instructionPlacement;

        return $this;
    }

    /**
     * @param array $hideOnScreen
     *
     * @return $this
     */
    public function setHideOnScreen(array $hideOnScreen): self
    {
        $this->hideOnScreen = $hideOnScreen;

        return $this;
    }
}
