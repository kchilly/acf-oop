<?php

declare(strict_types=1);

namespace Csoft\AcfOOP\Field;


use JsonSerializable;

interface FieldInterface extends JsonSerializable
{
}
