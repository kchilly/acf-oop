<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAppend;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;
use Csoft\AcfOOP\Field\Traits\FieldWithPrepend;

class EmailField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithPrepend;
    use FieldWithAppend;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'email';
    }
}
