<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;

class UrlField extends AbstractField
{
    use FieldWithPlaceholderTrait;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'url';
    }
}
