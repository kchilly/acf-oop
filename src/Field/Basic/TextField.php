<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAppend;
use Csoft\AcfOOP\Field\Traits\FieldWithDisabled;
use Csoft\AcfOOP\Field\Traits\FieldWithMaxLength;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;
use Csoft\AcfOOP\Field\Traits\FieldWithPrepend;
use Csoft\AcfOOP\Field\Traits\FieldWithReadOnly;

class TextField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithMaxLength;
    use FieldWithReadOnly;
    use FieldWithDisabled;
    use FieldWithPrepend;
    use FieldWithAppend;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'text';
    }
}
