<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use JsonSerializable;
use PhpWedge\Core\JsonSerializable\JsonSerializableTrait;

class FieldWrapper implements JsonSerializable
{
    use JsonSerializableTrait;

    private string $id;
    private string $class;
    private string $width;

    public function __construct(string $id = '', string $class = '', string $width = '')
    {
        $this->id = $id;
        $this->class = $class;
        $this->width = $width;
    }
}
