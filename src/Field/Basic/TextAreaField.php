<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithDisabled;
use Csoft\AcfOOP\Field\Traits\FieldWithMaxLength;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;
use Csoft\AcfOOP\Field\Traits\FieldWithReadOnly;
use Csoft\AcfOOP\Traits\ValidateValueTrait;

class TextAreaField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithMaxLength;
    use FieldWithReadOnly;
    use FieldWithDisabled;

    use ValidateValueTrait;

    /**
     * New Lines Possible Value Constants
     */
    const NEW_LINES_WPAUTOP = 'wpautop';
    const NEW_LINES_BR = 'br';
    const NEW_LINES_NO_FORMATTING = '';

    private int $rows;

    private string $newLines;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'textarea';
    }

    /**
     * Sets the number of rows and height.
     *
     * @param int $rows
     *
     * @return $this
     */
    public function setRows(int $rows): self
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * @param string $newLines
     *
     * @return $this
     */
    public function setNewLines(string $newLines): self
    {
        $available = [
            self::NEW_LINES_WPAUTOP,
            self::NEW_LINES_BR,
            self::NEW_LINES_NO_FORMATTING,
        ];

        $this->validateValue('new lines', $newLines, $available);
        $this->newLines = $newLines;

        return $this;
    }

    public function setNewLinesToWpautop(): self
    {
        $this->newLines = self::NEW_LINES_WPAUTOP;

        return $this;
    }

    public function setNewLinesToBr(): self
    {
        $this->newLines = self::NEW_LINES_BR;

        return $this;
    }

    public function setNewLinesToNoFormatting(): self
    {
        $this->newLines = self::NEW_LINES_NO_FORMATTING;

        return $this;
    }
}
