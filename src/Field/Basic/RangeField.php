<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Basic;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAppend;
use Csoft\AcfOOP\Field\Traits\FieldWithNumber;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;
use Csoft\AcfOOP\Field\Traits\FieldWithPrepend;

class RangeField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithPrepend;
    use FieldWithAppend;
    use FieldWithNumber;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'range';
    }
}
