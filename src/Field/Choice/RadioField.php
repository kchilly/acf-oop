<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Choice;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithChoices;
use Csoft\AcfOOP\Field\Traits\FieldWithLayout;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;

class RadioField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithChoices;
    use FieldWithLayout;

    private bool $otherChoice = false;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'radio';
    }

    /**
     * Sets the radio field to allow an "other" choice to be added by the user via text input.
     *
     * @return $this
     */
    public function allowOtherChoice(): self
    {
        $this->otherChoice = true;

        return $this;
    }
}
