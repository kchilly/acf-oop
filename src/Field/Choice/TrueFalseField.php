<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Choice;


use Csoft\AcfOOP\Field\AbstractField;

class TrueFalseField extends AbstractField
{
    private string $message;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'true_false';
    }

    /**
     * Sets the message to display next to the checkbox.
     *
     * @param string $message
     *
     * @return $this
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
