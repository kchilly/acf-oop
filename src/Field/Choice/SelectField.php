<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Choice;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAllowNull;
use Csoft\AcfOOP\Field\Traits\FieldWithChoices;
use Csoft\AcfOOP\Field\Traits\FieldWithMultiple;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;

class SelectField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithChoices;
    use FieldWithAllowNull;
    use FieldWithMultiple;

    private bool $ui = false;

    private bool $ajax = false;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'select';
    }

    /**
     * Sets the select field to use the select2 interface.
     *
     * @return $this
     */
    public function useUI(): self
    {
        $this->ui = true;

        return $this;
    }

    /**
     * Sets the select field to use ajax to load choices and also enables the select2 interface.
     *
     * @return $this
     */
    public function useAjax(): self
    {
        $this->useUI();
        $this->ajax = true;

        return $this;
    }
}
