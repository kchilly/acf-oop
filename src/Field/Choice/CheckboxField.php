<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Choice;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithChoices;
use Csoft\AcfOOP\Field\Traits\FieldWithLayout;
use Csoft\AcfOOP\Field\Traits\FieldWithPlaceholderTrait;

class CheckboxField extends AbstractField
{
    use FieldWithPlaceholderTrait;
    use FieldWithChoices;
    use FieldWithLayout;

    private bool $allowCustom;

    private bool $saveCustom;

    private bool $toggle;

    private string $returnFormat;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'checkbox';
    }

    /**
     * Sets the checkbox field to allow custom options to be added by the user.
     *
     * @return $this
     */
    public function allowCustom(): self
    {
        $this->allowCustom = true;

        return $this;
    }

    /**
     * Sets the checkbox field to allow custom options to be saved to the field choices.
     *
     * @return $this
     */
    public function allowSaveCustom(): self
    {
        $this->saveCustom = true;

        return $this;
    }

    /**
     * Adds a "Toggle all" checkbox to the list.
     *
     * @return $this
     */
    public function addToggleAll(): self
    {
        $this->toggle = true;

        return $this;
    }

    /**
     * Sets the return format to the value of the checkbox.
     *
     * @return $this
     */
    public function setReturnFormatToValue(): self
    {
        $this->returnFormat = 'value';

        return $this;
    }

    /**
     * Sets the return format to the label of the checkbox.
     *
     * @return $this
     */
    public function setReturnFormatToLabel(): self
    {
        $this->returnFormat = 'label';

        return $this;
    }

    /**
     * Sets the return format to the array of values of the checkbox.
     *
     * @return $this
     */
    public function setReturnFormatToArray(): self
    {
        $this->returnFormat = 'array';

        return $this;
    }
}
