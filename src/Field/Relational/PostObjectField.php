<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Relational;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAllowNull;
use Csoft\AcfOOP\Field\Traits\FieldWithMultiple;
use Csoft\AcfOOP\Field\Traits\FieldWithPostType;
use Csoft\AcfOOP\Field\Traits\FieldWithTaxonomy;

class PostObjectField extends AbstractField
{
    use FieldWithAllowNull;
    use FieldWithMultiple;
    use FieldWithPostType;
    use FieldWithTaxonomy;

    private string $returnFormat;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'post_object';
    }

    /**
     * Sets the return format to post object.
     *
     * @return $this
     */
    public function setReturnFormatToObject(): self
    {
        $this->returnFormat = 'object';

        return $this;
    }

    /**
     * Sets the return format to post id.
     *
     * @return $this
     */
    public function setReturnFormatToId(): self
    {
        $this->returnFormat = 'id';

        return $this;
    }
}
