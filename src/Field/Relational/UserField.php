<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Relational;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAllowNull;
use Csoft\AcfOOP\Field\Traits\FieldWithMultiple;

class UserField extends AbstractField
{
    use FieldWithAllowNull;
    use FieldWithMultiple;

    private array $role = [];

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'user';
    }

    /**
     * Adds a role to limit the users available for selection.
     *
     * @param string $role
     *
     * @return $this
     */
    public function addRole(string $role): self
    {
        $this->role[] = $role;

        return $this;
    }
}
