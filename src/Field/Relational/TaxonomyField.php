<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Relational;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAllowNull;

class TaxonomyField extends AbstractField
{
    use FieldWithAllowNull;

    private string $taxonomy = 'category';
    private string $fieldType;
    private bool $loadSaveTerms = false;
    private bool $addTerm = false;

    private string $returnFormat;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'taxonomy';
    }

    /**
     * Specifies the taxonomy to select terms from. Defaults to 'category'.
     *
     * @param string $taxonomy
     *
     * @return $this
     */
    public function setTaxonomy(string $taxonomy): self
    {
        $this->taxonomy = $taxonomy;

        return $this;
    }

    /**
     * Specifies the appearance of the taxonomy field to checkbox.
     *
     * @return $this
     */
    public function setFieldTypeToCheckbox(): self
    {
        $this->fieldType = 'checkbox';

        return $this;
    }

    /**
     * Specifies the appearance of the taxonomy field to radio.
     *
     * @return $this
     */
    public function setFieldTypeToRadio(): self
    {
        $this->fieldType = 'radio';

        return $this;
    }

    /**
     * Specifies the appearance of the taxonomy field to select.
     *
     * @return $this
     */
    public function setFieldTypeToSelect(): self
    {
        $this->fieldType = 'select';

        return $this;
    }

    /**
     * Specifies the appearance of the taxonomy field to multi select.
     *
     * @return $this
     */
    public function setFieldTypeToMultiSelect(): self
    {
        $this->fieldType = 'multi_select';

        return $this;
    }

    /**
     * Allows selected terms to be saved as relationships to the post.
     *
     * @return $this
     */
    public function loadSaveTerms(): self
    {
        $this->loadSaveTerms = true;

        return $this;
    }

    /**
     * Allow new terms to be added via a popup window.
     *
     * @return $this
     */
    public function addTerms(): self
    {
        $this->addTerm = true;

        return $this;
    }

    /**
     * Sets the return format to term object.
     *
     * @return $this
     */
    public function setReturnFormatToObject(): self
    {
        $this->returnFormat = 'object';

        return $this;
    }

    /**
     * Sets the return format to term id.
     *
     * @return $this
     */
    public function setReturnFormatToId(): self
    {
        $this->returnFormat = 'id';

        return $this;
    }
}
