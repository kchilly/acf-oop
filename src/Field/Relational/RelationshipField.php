<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Relational;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithMinMax;
use Csoft\AcfOOP\Field\Traits\FieldWithPostType;
use Csoft\AcfOOP\Field\Traits\FieldWithTaxonomy;

class RelationshipField extends AbstractField
{
    use FieldWithPostType;
    use FieldWithTaxonomy;
    use FieldWithMinMax;

    private array $filters = [];

    private array $elements = [];

    private string $returnFormat;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'relationship';
    }

    /**
     * Adds a search input filter.
     *
     * @return $this
     */
    public function addSearchFilter(): self
    {
        $this->filters[] = 'search';

        return $this;
    }

    /**
     * Adds a post type select field filter.
     *
     * @return $this
     */
    public function addPostTypeFilter(): self
    {
        $this->filters[] = 'post_type';

        return $this;
    }

    /**
     * Adds a taxonomy select field filter.
     *
     * @return $this
     */
    public function addTaxonomyFilter(): self
    {
        $this->filters[] = 'taxonomy';

        return $this;
    }

    /**
     * Adds a visual element to each posts.
     *
     * @param string $element
     *
     * @return $this
     */
    public function addElement(string $element): self
    {
        $this->elements[] = $element;

        return $this;
    }

    /**
     * Adds the featured image visual element to each post.
     *
     * @return $this
     */
    public function addFeaturedImageElement(): self
    {
        return $this->addElement('featured_image');
    }

    /**
     * Sets the return format to post object.
     *
     * @return $this
     */
    public function setReturnFormatToObject(): self
    {
        $this->returnFormat = 'object';

        return $this;
    }

    /**
     * Sets the return format to post id.
     *
     * @return $this
     */
    public function setReturnFormatToId(): self
    {
        $this->returnFormat = 'id';

        return $this;
    }
}
