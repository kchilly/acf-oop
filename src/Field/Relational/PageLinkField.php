<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Relational;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAllowNull;
use Csoft\AcfOOP\Field\Traits\FieldWithMultiple;
use Csoft\AcfOOP\Field\Traits\FieldWithPostType;
use Csoft\AcfOOP\Field\Traits\FieldWithTaxonomy;

class PageLinkField extends AbstractField
{
    use FieldWithAllowNull;
    use FieldWithMultiple;
    use FieldWithPostType;
    use FieldWithTaxonomy;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'page_link';
    }
}
