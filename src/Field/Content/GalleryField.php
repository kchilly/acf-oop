<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Content;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAttachment;
use Csoft\AcfOOP\Field\Traits\FieldWithImage;
use Csoft\AcfOOP\Traits\ValidateValueTrait;

class GalleryField extends AbstractField
{
    use FieldWithAttachment;
    use FieldWithImage;
    use ValidateValueTrait;

    private int $min = 0;
    private int $max = 0;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'gallery';
    }

    /**
     * Sets the minimum number of images required to be selected.
     *
     * @param int $min
     *
     * @return $this
     */
    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Sets the maximum number of images allowed to be selected.
     *
     * @param int $max
     *
     * @return $this
     */
    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }
}
