<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Content;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\Traits\FieldWithAttachment;
use Csoft\AcfOOP\Field\Traits\FieldWithImage;
use Csoft\AcfOOP\Traits\ValidateValueTrait;

class ImageField extends AbstractField
{
    use FieldWithAttachment;
    use FieldWithImage;
    use ValidateValueTrait;

    /** @var string Image Array */
    public const RETURN_FORMAT_ARRAY = 'array';
    /** @var string Image Url */
    public const RETURN_FORMAT_URL = 'url';
    /** @var string Image ID */
    public const RETURN_FORMAT_ID = 'id';

    private string $returnFormat;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'image';
    }

    /**
     * Sets the return format of the field.
     *
     * @param string $returnFormat
     *
     * @return $this
     */
    public function setReturnFormat(string $returnFormat): self
    {
        $available = [
            static::RETURN_FORMAT_ARRAY,
            static::RETURN_FORMAT_URL,
            static::RETURN_FORMAT_ID,
        ];

        $this->validateValue('return format', $returnFormat, $available);
        $this->returnFormat = $returnFormat;

        return $this;
    }

    /**
     * Sets the return format of the field to image array.
     *
     * @return $this
     */
    public function setReturnFormatToArray(): self
    {
        return $this->setReturnFormat(static::RETURN_FORMAT_ARRAY);
    }

    /**
     * Sets the return format of the field to image url.
     *
     * @return $this
     */
    public function setReturnFormatToUrl(): self
    {
        return $this->setReturnFormat(static::RETURN_FORMAT_URL);
    }

    /**
     * Sets the return format of the field to image id.
     *
     * @return $this
     */
    public function setReturnFormatToId(): self
    {
        return $this->setReturnFormat(static::RETURN_FORMAT_ID);
    }
}
