<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Content;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Traits\ValidateValueTrait;

class WYSIWYGField extends AbstractField
{
    use ValidateValueTrait;

    /** @var string Visual & Text */
    public const TABS_ALL = 'all';
    /** @var string Visual Only */
    public const TABS_VISUAL = 'visual';
    /** @var string Text Only */
    public const TABS_TEXT = 'text';

    /** @var string Full toolbar */
    public const TOOLBAR_FULL = 'full';
    /** @var string Basic toolbar */
    public const TOOLBAR_BASIC = 'basic';

    private string $tabs;

    private string $toolbar;

    private bool $mediaUpload = true;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'wysiwyg';
    }

    /**
     * Sets which tabs are available.
     *
     * @param string $tabs
     *
     * @return $this
     */
    public function setTabs(string $tabs): self
    {
        $available = [
            static::TABS_ALL,
            static::TABS_VISUAL,
            static::TABS_TEXT,
        ];

        $this->validateValue('tabs', $tabs, $available);
        $this->tabs = $tabs;

        return $this;
    }

    /**
     * Sets tab to show both visual and text. (Visual & Text)
     *
     * @return $this
     */
    public function setTabsToAll(): self
    {
        return $this->setTabs(static::TABS_ALL);
    }

    /**
     * Sets tab to show visual only. (Visual Only)
     *
     * @return $this
     */
    public function setTabsToVisual(): self
    {
        return $this->setTabs(static::TABS_VISUAL);
    }

    /**
     * Sets tab to show text only. (Text Only)
     *
     * @return $this
     */
    public function setTabsToText(): self
    {
        return $this->setTabs(static::TABS_TEXT);
    }

    /**
     * Sets the editor's toolbar.
     * Choices of 'full' (Full), 'basic' (Basic) or a custom toolbar (https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/)
     *
     * @param string $toolbar
     *
     * @return $this
     */
    public function setToolbar(string $toolbar): self
    {
        $this->toolbar = $toolbar;

        return $this;
    }

    /**
     * Sets toolbar to full. (Full toolbar
     *
     * @return $this
     */
    public function setToolbarToFull(): self
    {
        return $this->setToolbar(static::TOOLBAR_FULL);
    }

    /**
     * Sets toolbar to basic. (Basic toolbar)
     *
     * @return $this
     */
    public function setToolbarToBasic(): self
    {
        return $this->setToolbar(static::TOOLBAR_BASIC);
    }

    /**
     * Sets the media upload button hidden.
     *
     * @return $this
     */
    public function disableMediaUpload(): self
    {
        $this->mediaUpload = false;

        return $this;
    }
}
