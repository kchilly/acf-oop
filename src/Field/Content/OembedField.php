<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Content;


use Csoft\AcfOOP\Field\AbstractField;

class OembedField extends AbstractField
{
    private int $width;

    private int $height;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'oembed';
    }

    /**
     * Sets the width of the oEmbed element.
     *
     * @param int $width
     *
     * @return $this
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Sets the height of the oEmbed element.
     *
     * @param int $height
     *
     * @return $this
     */
    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }
}
