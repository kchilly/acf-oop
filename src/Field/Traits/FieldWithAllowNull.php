<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithAllowNull
{
    private bool $allowNull = false;

    /**
     * Sets the field to allow null values.
     *
     * @return $this
     */
    public function setAllowNull(): self
    {
        $this->allowNull = true;

        return $this;
    }
}
