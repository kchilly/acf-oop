<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithPrepend
{
    private string $prepend;

    /**
     * Set the prepend value appearing before the input.
     *
     * @param string $prepend
     *
     * @return $this
     */
    public function setPrepend(string $prepend): self
    {
        $this->prepend = $prepend;

        return $this;
    }
}
