<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithChoices
{
    private array $choices = [];

    /**
     * Sets the choices for the select field.
     *
     * @param array $choices Key is used as value and value as label. ([key => value])
     *
     * @return $this
     */
    public function setChoices(array $choices): self
    {
        $this->choices = $choices;

        return $this;
    }

    /**
     * Adds a choice to the select field.
     *
     * @param string $key Used as value.
     * @param string $value Used as label.
     *
     * @return $this
     */
    public function addChoice(string $key, string $value): self
    {
        $this->choices[$key] = $value;

        return $this;
    }
}
