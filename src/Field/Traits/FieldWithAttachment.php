<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithAttachment
{
    private string $previewSize;

    private string $library;

    private int|string $minSize;

    private int|string $maxSize;

    private string $mimeTypes;

    /**
     * Specify the image size shown when editing. Defaults to 'thumbnail'.
     *
     * @param string $previewSize
     *
     * @return $this
     */
    public function setPreviewSize(string $previewSize = 'thumbnail'): self
    {
        $this->previewSize = $previewSize;

        return $this;
    }

    /**
     * Restrict the image library to all images.
     *
     * @return $this
     */
    public function setLibraryToAll(): self
    {
        $this->library = 'all';

        return $this;
    }

    /**
     * Restrict the image library to uploaded to post.
     *
     * @return $this
     */
    public function setLibraryToUploadedToPost(): self
    {
        $this->library = 'uploadedTo';

        return $this;
    }

    /**
     * Specify the minimum filesize in MB required when uploading.
     * The unit may also be included. e.g. '256KB'.
     *
     * @param int|string $minSize
     *
     * @return $this
     */
    public function setMinSize(int|string $minSize): self
    {
        $this->minSize = $minSize;

        return $this;
    }

    /**
     * Specify the maximum filesize in MB in px allowed when uploading.
     * The unit may also be included. e.g. '256KB'.
     *
     * @param int|string $maxSize
     *
     * @return $this
     */
    public function setMaxSize(int|string $maxSize): self
    {
        $this->maxSize = $maxSize;

        return $this;
    }

    /**
     * Set the allowed mime types when uploading.
     * MimeType constants are available in the PhpWedge\Core\MimeType class.
     *
     * @param array $mimeTypes
     *
     * @return $this
     */
    public function setMimeTypes(array $mimeTypes): self
    {
        $this->mimeTypes = implode(',', $mimeTypes);

        return $this;
    }
}
