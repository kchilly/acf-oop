<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithTaxonomy
{
    private array $taxonomy = [];

    /**
     * Adds a taxonomy to filter the available choices.
     *
     * @param string $taxonomy
     *
     * @return $this
     */
    public function addTaxonomy(string $taxonomy): self
    {
        $this->taxonomy[] = $taxonomy;

        return $this;
    }
}
