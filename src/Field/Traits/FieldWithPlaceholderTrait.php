<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithPlaceholderTrait
{
    private string $placeholder;

    /**
     * Sets the placeholder value appearing within the input.
     *
     * @param string $placeholder
     *
     * @return $this
     */
    public function setPlaceholder(string $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }
}
