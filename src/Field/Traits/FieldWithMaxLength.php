<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithMaxLength
{
    private string $maxlength;

    /**
     * Restricts the character limit.
     *
     * @param string $maxlength
     *
     * @return $this
     */
    public function setMaxlength(string $maxlength): self
    {
        $this->maxlength = $maxlength;

        return $this;
    }
}
