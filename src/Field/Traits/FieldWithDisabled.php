<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithDisabled
{
    private bool $disabled = false;

    /**
     * Makes the input disabled.
     *
     * @return $this
     */
    public function setDisabled(): self
    {
        $this->disabled = true;

        return $this;
    }
}
