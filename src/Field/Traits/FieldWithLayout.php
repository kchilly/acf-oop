<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithLayout
{
    private string $layout;

    /**
     * Sets vertical layout of the checkbox inputs.
     *
     * @return $this
     */
    public function setLayoutToVertical(): self
    {
        $this->layout = 'vertical';

        return $this;
    }

    /**
     * Sets horizontal layout of the checkbox inputs.
     *
     * @return $this
     */
    public function setLayoutToHorizontal(): self
    {
        $this->layout = 'horizontal';

        return $this;
    }
}
