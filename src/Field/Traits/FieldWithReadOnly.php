<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithReadOnly
{
    private bool $readonly = false;

    /**
     * Makes the input readonly.
     *
     * @return $this
     */
    public function setReadonly(): self
    {
        $this->readonly = true;

        return $this;
    }
}
