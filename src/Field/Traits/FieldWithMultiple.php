<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithMultiple
{
    private bool $multiple = false;

    /**
     * Sets the field to allow multiple values.
     *
     * @return $this
     */
    public function allowMultiple(): self
    {
        $this->multiple = true;

        return $this;
    }
}
