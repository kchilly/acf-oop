<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithMinMax
{
    private int $min;

    private int $max;

    /**
     * Sets the minimum number or minimum element value.
     *
     * @param int $min
     *
     * @return $this
     */
    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Sets the maximum number or maximum element value.
     *
     * @param int $max
     *
     * @return $this
     */
    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }
}
