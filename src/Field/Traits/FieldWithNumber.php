<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithNumber
{
    use FieldWithMinMax;

    private int $step;

    /**
     * Sets the step size increments.
     *
     * @param int $step
     *
     * @return $this
     */
    public function setStep(int $step): self
    {
        $this->step = $step;

        return $this;
    }
}
