<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithAppend
{
    private string $append;

    /**
     * Set the append value appearing after the input.
     *
     * @param string $append
     *
     * @return $this
     */
    public function setAppend(string $append): self
    {
        $this->append = $append;

        return $this;
    }
}
