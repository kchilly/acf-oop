<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithPostType
{
    private array $postType = [];

    /**
     * Adds a post type to filter the available choices.
     *
     * @param string $postType
     *
     * @return $this
     */
    public function addPostType(string $postType): self
    {
        $this->postType[] = $postType;

        return $this;
    }
}
