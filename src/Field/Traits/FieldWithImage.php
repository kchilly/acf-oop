<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Traits;


trait FieldWithImage
{
    private int $minWidth;

    private int $minHeight;

    private int $maxWidth;

    private int $maxHeight;

    /**
     * Specify the minimum width in px required when uploading.
     *
     * @param int $minWidth
     *
     * @return $this
     */
    public function setMinWidth(int $minWidth): self
    {
        if (isset($this->maxWidth)) {
            $this->validateIntRange(
                'min width',
                $minWidth,
                'max width',
                $this->maxWidth
            );
        }

        $this->minWidth = $minWidth;

        return $this;
    }

    /**
     * Specify the minimum height in px required when uploading.
     *
     * @param int $minHeight
     *
     * @return $this
     */
    public function setMinHeight(int $minHeight): self
    {
        if (isset($this->maxHeight)) {
            $this->validateIntRange(
                'min height',
                $minHeight,
                'max height',
                $this->maxHeight
            );
        }

        $this->minHeight = $minHeight;

        return $this;
    }

    /**
     * Specify the maximum width in px allowed when uploading.
     *
     * @param int $maxWidth
     *
     * @return $this
     */
    public function setMaxWidth(int $maxWidth): self
    {
        if (isset($this->minWidth)) {
            $this->validateIntRange(
                'min width',
                $this->minWidth,
                'max width',
                $maxWidth
            );
        }

        $this->maxWidth = $maxWidth;

        return $this;
    }

    /**
     * Specify the maximum height in px allowed when uploading.
     *
     * @param int $maxHeight
     *
     * @return $this
     */
    public function setMaxHeight(int $maxHeight): self
    {
        if (isset($this->minHeight)) {
            $this->validateIntRange(
                'min height',
                $this->minHeight,
                'max height',
                $maxHeight
            );
        }

        $this->maxHeight = $maxHeight;

        return $this;
    }
}
