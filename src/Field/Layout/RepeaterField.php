<?php

declare(strict_types=1);


namespace Csoft\AcfOOP\Field\Layout;


use Csoft\AcfOOP\Field\AbstractField;
use Csoft\AcfOOP\Field\FieldInterface;

class RepeaterField extends AbstractField
{
    /** @var FieldInterface[] */
    private array $subFields = [];
    private bool $collapsed;
    private int $min;
    private int $max;
    private string $layout;
    private string $buttonLabel;

    /**
     * @inheritDoc
     */
    public function getTypeName(): string
    {
        return 'repeater';
    }

    /**
     * Adds a sub-field to the repeater.
     *
     * @param FieldInterface $field
     *
     * @return $this
     */
    public function addSubField(FieldInterface $field): self
    {
        $this->subFields[] = $field;

        return $this;
    }

    /**
     * Sets the repeater to be collapsed by default.
     *
     * @return $this
     */
    public function collapse(): self
    {
        $this->collapsed = true;

        return $this;
    }

    /**
     * Sets the minimum number of rows required.
     *
     * @param int $min
     *
     * @return $this
     */
    public function setMin(int $min): self
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Sets the maximum number of rows allowed.
     *
     * @param int $max
     *
     * @return $this
     */
    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Sets the layout to table. Sub-fields are displayed in a table. Labels will appear in the table header.
     *
     * @return $this
     */
    public function setLayoutToTable(): self
    {
        $this->layout = 'table';

        return $this;
    }

    /**
     * Sets the layout to block. Sub-fields are displayed in blocks, one after the other.
     *
     * @return $this
     */
    public function setLayoutToBlock(): self
    {
        $this->layout = 'block';

        return $this;
    }

    /**
     * Sets the layout to row. Sub-fields are displayed in a two column table. Labels will appear in the first column.
     *
     * @return $this
     */
    public function setLayoutToRow(): self
    {
        $this->layout = 'row';

        return $this;
    }

    /**
     * Sets the label for the add row button.
     *
     * @param string $label
     *
     * @return $this
     */
    public function setButtonLabel(string $label): self
    {
        $this->buttonLabel = $label;

        return $this;
    }
}
