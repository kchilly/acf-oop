<?php

declare(strict_types=1);

namespace Csoft\AcfOOP\Field;


use Csoft\AcfOOP\Field\Basic\FieldWrapper;
use InvalidArgumentException;
use PhpWedge\Core\JsonSerializable\JsonSerializableSnakeCaseTrait;

abstract class AbstractField implements FieldInterface
{
    use JsonSerializableSnakeCaseTrait;

    private const KEY_PREFIX = 'field_';

    protected string $key;
    protected string $name;
    protected string $label;
    protected string $type;
    protected string $instructions;
    protected bool $required = false;
    protected $conditionalLogic;
    protected $defaultValue;
    protected ?FieldWrapper $wrapper;

    public function __construct(string $name, string $label)
    {
        $this->key = self::KEY_PREFIX . $name;
        $this->name = $name;
        $this->label = $label;
        $this->type = $this->getTypeName();

        $this->validateName();
    }

    /**
     * Returns the actual type name.
     *
     * @return string
     */
    abstract public function getTypeName(): string;

    /**
     * Set instructions for authors. Shown when submitting data.
     *
     * @param string $instructions
     *
     * @return $this
     */
    public function setInstructions(string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Sets the field value is required
     *
     * @return $this
     */
    public function setRequired(): self
    {
        $this->required = true;

        return $this;
    }

    /**
     * Conditionally hide or show this field based on other field's values.
     * Best to use the ACF UI and export to understand the array structure.
     *
     * @param mixed $conditionalLogic
     *
     * @return $this
     */
    public function setConditionalLogic($conditionalLogic): self
    {
        $this->conditionalLogic = $conditionalLogic;

        return $this;
    }

    /**
     * Sets default value used by ACF if no value has yet been saved
     *
     * @param mixed $defaultValue
     *
     * @return $this
     */
    public function setDefaultValue($defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @param FieldWrapper|null $wrapper
     *
     * @return $this
     */
    public function setWrapper(?FieldWrapper $wrapper): self
    {
        $this->wrapper = $wrapper;

        return $this;
    }

    /**
     * Validates the name.
     *
     * @throws InvalidArgumentException
     */
    private function validateName(): void
    {
        if (preg_match('#^[a-z-_]+$#i', $this->name, $matches) === 0) {
            throw new InvalidArgumentException('The name must be a single word without special character, except "-" or "_" as whitespace!');
        }
    }
}
