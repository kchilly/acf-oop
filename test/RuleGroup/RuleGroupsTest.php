<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\RuleGroup;


use Csoft\AcfOOP\Rule\BlockTypeRule;
use Csoft\AcfOOP\Rule\PostTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;
use Csoft\AcfOOP\RuleGroup\RuleGroup;
use Csoft\AcfOOP\RuleGroup\RuleGroups;
use PHPUnit\Framework\TestCase;

class RuleGroupsTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $rule1 = new BlockTypeRule(RuleInterface::EQUALS, 'block_name');
        $rule2 = new PostTypeRule(RuleInterface::NOT_EQUALS, 'post_name');

        $ruleGroup1 = (new RuleGroup())->addRule($rule1)
            ->addRule($rule2);
        $ruleGroup2 = (new RuleGroup())->addRule($rule2);

        $ruleGroups = (new RuleGroups())->addRuleGroup($ruleGroup1)
            ->addRuleGroup($ruleGroup2);

        $expected = [
            [
                [
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block_name',
                ],
                [
                    'param' => 'post_type',
                    'operator' => '!=',
                    'value' => 'post_name',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '!=',
                    'value' => 'post_name',
                ],
            ],
        ];

        $this->assertEquals($expected, $ruleGroups->jsonSerialize());
    }
}
