<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Rule;


use Csoft\AcfOOP\Rule\BlockTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;
use PHPUnit\Framework\TestCase;

class BlockTypeRuleTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $rule = new BlockTypeRule(RuleInterface::EQUALS, 'block_name');
        $expected = [
            'param' => 'block',
            'operator' => '==',
            'value' => 'acf/block_name',
        ];

        $this->assertEquals($expected, $rule->jsonSerialize());
    }
}
