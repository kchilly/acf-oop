<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Rule;


use Csoft\AcfOOP\Rule\PostTypeRule;
use Csoft\AcfOOP\Rule\RuleInterface;
use PHPUnit\Framework\TestCase;

class PostTypeRuleTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $rule = new PostTypeRule(RuleInterface::NOT_EQUALS, 'block_name');
        $expected = [
            'param' => 'post_type',
            'operator' => '!=',
            'value' => 'block_name',
        ];

        $this->assertEquals($expected, $rule->jsonSerialize());
    }
}
