<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field;


use Csoft\AcfOOP\Field\Basic\FieldWrapper;
use Csoft\AcfOOP\Field\FieldInterface;
use PHPUnit\Framework\TestCase;

abstract class AbstractFieldTestCaseWithFixtureMethods extends TestCase
{
    /**
     * Fills in the generic settings properties with fixtures on the given field instance.
     *
     * @param FieldInterface $field
     */
    public function fillGenericSettingPropertiesWithFixtures(FieldInterface $field): void
    {
        $field->setInstructions('These are the instructions')
            ->setRequired()
            ->setConditionalLogic('whatever')
            ->setDefaultValue('This is the default value')
            ->setWrapper(
                new FieldWrapper('id1', 'class1', '100px')
            );
    }

    /**
     * Returns the used generic settings fixtures.
     *
     * @return array
     */
    public function getGenericSettingFixtures(): array
    {
        return [
            'key' => 'field_hello_world',
            'name' => 'hello_world',
            'label' => 'Hello World!',
            'instructions' => 'These are the instructions',
            'required' => true,
            'conditional_logic' => 'whatever',
            'default_value' => 'This is the default value',
            'wrapper' => [
                'id' => 'id1',
                'class' => 'class1',
                'width' => '100px',
            ],
        ];
    }
}
