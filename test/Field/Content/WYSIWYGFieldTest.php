<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Content;


use Csoft\AcfOOP\Field\Content\WYSIWYGField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;
use InvalidArgumentException;

class WYSIWYGFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testInvalidNewLines(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" tabs is not valid! Available: "all", "visual", "text"');

        (new WYSIWYGField('hello-world', 'Hello World!'))->setTabs('invalid');
    }

    public function testEveryProperty(): void
    {
        $field = new WYSIWYGField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setTabs(WYSIWYGField::TABS_TEXT)
            ->setToolbar(WYSIWYGField::TOOLBAR_FULL)
            ->disableMediaUpload();

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'wysiwyg',
                // Field specific properties
                'tabs' => 'text',
                'toolbar' => 'full',
                'media_upload' => false,
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
