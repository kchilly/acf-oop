<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Content;


use Csoft\AcfOOP\Field\Content\OembedField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;

class OembedFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testEveryProperty(): void
    {
        $field = new OembedField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setWidth(100)
            ->setHeight(50);

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'oembed',
                // Field specific properties
                'width' => 100,
                'height' => 50,
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
