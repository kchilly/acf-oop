<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Content;


use Csoft\AcfOOP\Field\Content\ImageField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;
use InvalidArgumentException;
use PhpWedge\Core\MimeType;

class ImageFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testInvalidReturnFormat(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" return format is not valid! Available: "array", "url", "id"');

        (new ImageField('hello-world', 'Hello World!'))->setReturnFormat('invalid');
    }

    public function testInvalidWidthRangeMin(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max width value (100) needs to be greater than the min width value (200)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMaxWidth(100)
            ->setMinWidth(200);
    }

    public function testInvalidWidthRangeMax(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max width value (100) needs to be greater than the min width value (200)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMinWidth(200)
            ->setMaxWidth(100);
    }

    public function testInvalidHeightRangeMin(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max height value (100) needs to be greater than the min height value (200)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMaxHeight(100)
            ->setMinHeight(200);
    }

    public function testInvalidHeightRangeMax(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max height value (100) needs to be greater than the min height value (200)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMinHeight(200)
            ->setMaxHeight(100);
    }

    public function testInvalidSizeRangeMin(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max width value (1MB) needs to be greater than the min width value (4096KB)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMaxSize('1')
            ->setMinSize('4096KB');
    }

    public function testInvalidSizeRangeMax(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given max width value (1MB) needs to be greater than the min width value (4MB)!');

        (new ImageField('hello-world', 'Hello World!'))
            ->setMinSize('4096KB')
            ->setMaxSize('1');
    }

    public function testEveryProperty(): void
    {
        $field = new ImageField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setReturnFormat(ImageField::RETURN_FORMAT_ID)
            ->setPreviewSize('thumbnail')
            ->setLibraryToUploadedToPost()
            ->setMinWidth(200)
            ->setMinHeight(100)
            ->setMinSize('256KB')
            ->setMaxWidth(400)
            ->setMaxHeight(200)
            ->setMaxSize('20MB')
            ->setMimeTypes([MimeType::TYPE_WEBP, MimeType::TYPE_PNG, MimeType::TYPE_JPG, MimeType::TYPE_GIF]);

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'image',
                // Field specific properties
                'return_format' => 'id',
                'preview_size' => 'thumbnail',
                'library' => 'uploadedTo',
                'min_width' => 200,
                'min_height' => 100,
                'min_size' => '256KB',
                'max_width' => 400,
                'max_height' => 200,
                'max_size' => '20MB',
                'mime_types' => 'image/webp,image/png,image/jpeg,image/gif',
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
