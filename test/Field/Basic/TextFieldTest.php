<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\TextField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;
use InvalidArgumentException;

class TextFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testValidateNameForFailure(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The name must be a single word without special character, except "-" or "_" as whitespace!');

        new TextField('hello world!', 'Hello World!');
    }

    public function testEveryProperty(): void
    {
        $field = new TextField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setPlaceholder('Placeholder text')
            ->setPrepend('Prepend text')
            ->setAppend('Append text')
            ->setMaxlength('10')
            ->setReadonly()
            ->setDisabled();

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'text',
                // Field specific properties
                'placeholder' => 'Placeholder text',
                'prepend' => 'Prepend text',
                'append' => 'Append text',
                'maxlength' => '10',
                'readonly' => true,
                'disabled' => true,
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
