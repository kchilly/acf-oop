<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\PasswordField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;

class PasswordFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testEveryProperty(): void
    {
        $field = new PasswordField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setPlaceholder('Placeholder text')
            ->setPrepend('Prepend text')
            ->setAppend('Append text');

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'password',
                // Field specific properties
                'placeholder' => 'Placeholder text',
                'prepend' => 'Prepend text',
                'append' => 'Append text',
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
