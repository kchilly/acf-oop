<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\UrlField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;

class UrlFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testEveryProperty(): void
    {
        $field = new UrlField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setPlaceholder('Placeholder text');

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'url',
                // Field specific properties
                'placeholder' => 'Placeholder text',
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
