<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\TextAreaField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;
use InvalidArgumentException;

class TextAreaFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testInvalidNewLines(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" new lines is not valid! Available: "wpautop", "br", ""');

        (new TextAreaField('hello-world', 'Hello World!'))->setNewLines('invalid');
    }

    public function testEveryProperty(): void
    {
        $field = new TextAreaField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setPlaceholder('Placeholder text')
            ->setNewLines(TextAreaField::NEW_LINES_BR)
            ->setRows(5)
            ->setMaxlength('10')
            ->setReadonly()
            ->setDisabled();

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'textarea',
                // Field specific properties
                'placeholder' => 'Placeholder text',
                'new_lines' => 'br',
                'rows' => 5,
                'maxlength' => '10',
                'readonly' => true,
                'disabled' => true,
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
