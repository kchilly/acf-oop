<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\NumberField;
use Csoft\AcfOOPTest\Field\AbstractFieldTestCaseWithFixtureMethods;

class NumberFieldTest extends AbstractFieldTestCaseWithFixtureMethods
{
    public function testEveryProperty(): void
    {
        $field = new NumberField('hello_world', 'Hello World!');
        // Generic properties
        $this->fillGenericSettingPropertiesWithFixtures($field);

        // Field specific properties
        $field->setPlaceholder('Placeholder text')
            ->setPrepend('Prepend text')
            ->setAppend('Append text')
            ->setMin(4)
            ->setMax(8)
            ->setStep(2);

        $expected = array_merge(
            // Generic properties
            $this->getGenericSettingFixtures(),
            [
                'type' => 'number',
                // Field specific properties
                'placeholder' => 'Placeholder text',
                'prepend' => 'Prepend text',
                'append' => 'Append text',
                'min' => 4,
                'max' => 8,
                'step' => 2,
            ]
        );

        $this->assertEquals($expected, $field->jsonSerialize());
    }
}
