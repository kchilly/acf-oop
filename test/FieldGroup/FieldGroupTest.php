<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Field\Basic;


use Csoft\AcfOOP\Field\Basic\TextField;
use Csoft\AcfOOP\FieldGroup\FieldGroup;
use Csoft\AcfOOP\Location\PostTypeLocation;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class FieldGroupTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $fieldGroup = new FieldGroup(
            '1',
            'My Group',
            new PostTypeLocation('post_name')
        );
        $fieldGroup->setFields([new TextField('hello_world', 'Hello World!')])
            ->setMenuOrder(5)
            ->setPosition('normal')
            ->setStyle('default')
            ->setLabelPlacement('top')
            ->setInstructionPlacement('label')
            ->setHideOnScreen(['a']);

        $expected = [
            'key' => 'group_1',
            'title' => 'My Group',
            'fields' => [
                [
                    'key' => 'field_hello_world',
                    'name' => 'hello_world',
                    'label' => 'Hello World!',
                    'type' => 'text',
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post_name',
                    ],
                ],
            ],
            'menu_order' => 5,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => ['a'],
        ];

        $this->assertEquals($expected, $fieldGroup->jsonSerialize());
    }

    public function testInvalidPosition(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" position is not valid! Available: "normal", "side", "acf_after_title"');

        (new FieldGroup(
            '1',
            'My Group',
            new PostTypeLocation('post_name')
        ))->setPosition('invalid');
    }

    public function testInvalidStyle(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" style is not valid! Available: "default", "seamless"');

        (new FieldGroup(
            '1',
            'My Group',
            new PostTypeLocation('post_name')
        ))->setStyle('invalid');
    }

    public function testInvalidLabelPlacement(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" label placement is not valid! Available: "left", "top"');

        (new FieldGroup(
            '1',
            'My Group',
            new PostTypeLocation('post_name')
        ))->setLabelPlacement('invalid');
    }

    public function testInvalidInstructionPlacement(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The given "invalid" instruction placement is not valid! Available: "field", "label"');

        (new FieldGroup(
            '1',
            'My Group',
            new PostTypeLocation('post_name')
        ))->setInstructionPlacement('invalid');
    }
}
