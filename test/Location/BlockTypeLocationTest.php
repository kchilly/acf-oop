<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Location;


use Csoft\AcfOOP\Location\BlockTypeLocation;
use PHPUnit\Framework\TestCase;

class BlockTypeLocationTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $location = new BlockTypeLocation('block_name');
        $expected = [
            [
                [
                    'param' => 'block',
                    'operator' => '==',
                    'value' => 'acf/block_name',
                ],
            ],
        ];

        $this->assertEquals($expected, $location->jsonSerialize());
    }
}
