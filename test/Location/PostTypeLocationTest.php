<?php

declare(strict_types=1);


namespace Csoft\AcfOOPTest\Location;


use Csoft\AcfOOP\Location\PostTypeLocation;
use PHPUnit\Framework\TestCase;

class PostTypeLocationTest extends TestCase
{
    public function testEveryProperty(): void
    {
        $location = new PostTypeLocation('post_name');
        $expected = [
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post_name',
                ],
            ],
        ];

        $this->assertEquals($expected, $location->jsonSerialize());
    }
}
