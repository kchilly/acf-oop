# ACF OOP
> The package is making it possible to have a more controlled data model 
driven approach to use the ACF in oppose of the uncontrolled, complex arrays.

## Install


## Usage
### ACF Field Group declaration with array
```php
acf_add_local_field_group([
	'key' => 'group_1',
	'title' => 'My Group',
	'fields' => [
        'key' => 'field_name',
        'name' => 'name',
        'label' => 'label',
        'type' => 'text',
    ],
	'location' => [
		[
			[
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
                'required' => true,
			],
		],
	],
]);
```
### ACF Field Group declaration with this library
```php
use AcfOOP\Field\TextField;
use AcfOOP\FieldGroup\FieldGroup;
use AcfOOP\Location\PostTypeLocation;

$textField = (new TextField('name', 'label'))
    ->setRequired()
    ->setPlaceholder('This is the placeholder text');

$fieldGroup = (new FieldGroup(
    '1',
    'My group',
    new PostTypeLocation('post')
))->setFields([$textField]);

acf_add_local_field_group($fieldGroup->jsonSerialize());
```
